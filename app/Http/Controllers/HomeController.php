<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Postingan;
use App\Komentar;
use App\PostinganLike;
use App\KomentarLike;
use App\ReKomentar;
use App\FollowerUser;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $postingan = Postingan::all()->sortByDesc('created_at');
        $user = User::all();
        $komentar = Komentar::all();
        $rekomentar = ReKomentar::all();
        $profile=Profile::where('user_id',Auth::id())->get()->first();
        $postinganlike=PostinganLike::where('user_id', Auth::id())->get();
        $followinguserarray=FollowerUser::where('follower_id', Auth::id())->get('user_id')->toArray();
        $usercurrent=  User::where('id',Auth::id())->get()->first();
        
        //dd($postinganlike);
        $followinguser=array();
        $i=0;
        while ($i<sizeof($followinguserarray)){
            $followinguser[]=$followinguserarray[$i]['user_id'];
            $i++;
        }
        return view('index', compact('postingan','user','komentar','rekomentar','profile','postinganlike','followinguser','usercurrent'));

        //return view('home');
        //return view('index');
    }
}
