<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomentarLike;
use Illuminate\Support\Facades\Auth;


class KomentarLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function report(Exception $exception)
    {
        if ($exception instanceof CustomException) {
            //
        }

        parent::report($exception);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            // Validate the request...
            //dd($request);
            //
            $validated = $request->validate([
                'komentar_id' => 'required',
            ]);
         
            try {
                $komentarlike = new KomentarLike;
    
                $komentarlike->firstOrCreate(['user_id' => Auth::id(), 'komentar_id' =>$request->komentar_id ]);
                /*$postinganlike->user_id=Auth::id();
                $postinganlike->postingan_id=$request->postingan_id;
                $postinganlike->save();*/
            } catch (Exception $e) {
                report($e);        
            }
            return redirect()->back();
    
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
