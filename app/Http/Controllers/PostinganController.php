<?php
namespace App\Http\Controllers;
use App\Postingan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class PostinganController extends Controller
{

   /* public function __construct()
    {
        //$this->middleware('auth')->except(['index','show']);

        //$this->middleware('log')->only('index');

        //$this->middleware('subscribed')->except('store');

        $this->middleware('auth');
    }*/


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $postingan = Postingan::where('user_id',Auth::id())->get();
        return view('postingan.index', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        //
        //dd(Auth::id);

        $validated = $request->validate([
            'caption' => 'required|max:45',
            'gambar' => 'required|image|mimes:jpg,jpeg,png',
            'tulisan' => 'required',
        ]);
      
        $namaGambar=time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('gambar'),$namaGambar);
        
        $postingan=new Postingan;
        $postingan->caption=$request->caption;
        $postingan->gambar=$namaGambar;
        $postingan->tulisan=$request->tulisan;
        $postingan->user_id=Auth::id();
        if ($request->quote) $postingan->quote=$request->quote;
        
        $postingan->save();
        return redirect('/postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $postingan=Postingan::findOrFail($id);
        return view('postingan.show',compact('postingan'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $postingan=Postingan::findOrFail($id);
        return view('postingan.edit',compact('postingan'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
       //dd($request);
        
        $validated = $request->validate([
            'caption' => 'required|max:45',
            'gambar' => 'required|image|mimes:jpg,jpeg,png',
            'tulisan' => 'required',
            'user_id' => 'required',
        ]);
      
        if ($request->has('gambar')){
            $postingan=Postingan::find($id);
            $path="gambar/";
            Postingan::delete($path.$postingan->gambar);

            $namaGambar=time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('gambar'),$namaGambar);

            $postingan=Postingan::find($id);
            $postingan->caption=$request->caption;
            $postingan->gambar=$namaGambar;
            $postingan->tulisan=$request->tulisan;
            if ($request->quote) $postingan->quote=$request->quote;
            
            $postingan->save();
            
        }else{
            $postingan=Postingan::find($id);
            $postingan->caption=$request->caption;
            $postingan->gambar=$namaGambar;
            $postingan->tulisan=$request->tulisan;
            if ($request->quote) $postingan->quote=$request->quote;
            
            $postingan->save();

        }
        return redirect('/postingan'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $postingan=Postingan::find($id);
        $path="gambar/";
        File::delete($path.$postingan->gambar);
        $postingan->delete();

        return redirect('/postingan');
        
    }
}
