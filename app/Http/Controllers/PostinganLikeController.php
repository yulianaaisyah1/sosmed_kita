<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostinganLike;
use Illuminate\Support\Facades\Auth;


class PostinganLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function report(Exception $exception)
    {
        if ($exception instanceof CustomException) {
            //
        }

        parent::report($exception);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            // Validate the request...
            //dd($request);
            //
            $validated = $request->validate([
                'postingan_id' => 'required',
            ]);
         
            try {
                $postinganlike = new PostinganLike;
    
                $postinganlike->firstOrCreate(['user_id' => Auth::id(), 'postingan_id' =>$request->postingan_id ]);
                /*$postinganlike->user_id=Auth::id();
                $postinganlike->postingan_id=$request->postingan_id;
                $postinganlike->save();*/
            } catch (Exception $e) {
                report($e);        
            }
            return redirect()->back();
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
