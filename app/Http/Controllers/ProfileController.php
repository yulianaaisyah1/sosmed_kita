<?php

namespace App\Http\Controllers;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //
    public function index(){
        $profile= Profile::where('user_id',Auth::id())->first();
        return view('profile.index',compact('profile'));
    }
    public function update(Request $request,$id){
          //
        $validated = $request->validate([
            'umur' => 'required|integer',
            'alamat' => 'required',
            'bio' => 'required',
        ]);
        $profile=Profile::find($id);
        $profile->umur=$request->umur;
        $profile->alamat=$request->alamat;
        $profile->bio=$request->bio;
        $profile->save();            
        return redirect('/profile');      

    }
}
