<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarLike extends Model
{
    //
    protected $table='komentar_like';
    protected $fillable = [
        'komentar_id','user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->belongsTo('App\Komentar');
    }
}
