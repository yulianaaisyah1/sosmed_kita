<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    // 
    protected $table='postingan';
    protected $fillable = [
        'caption', 'gambar', 'tulisan','quote','jumlah_like','jumlah_komentar','user_id'
    ];

    public function komentar(){
        return $this->hasMany('App\Komentar');
    }

    public function postingan_like(){
        return $this->hasMany('App\PostinganLike');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
