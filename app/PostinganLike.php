<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostinganLike extends Model
{
    //
    protected $table='postingan_like';
    protected $fillable = [
        'postingan_id','user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }

}
