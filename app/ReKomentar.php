<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReKomentar extends Model
{
    //
    protected $table='re_komentar';
    protected $fillable = [
        're_komentar', 'user_id', 'komentar_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->belongsTo('App\Komentar');
    }
}
