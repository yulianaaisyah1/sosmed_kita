<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPrimaryAddNewPrimaryToPostinganLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('postingan_like', function (Blueprint $table) {
            //
            $table->dropColumn('id');
            $table->primary(['user_id', 'postingan_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('postingan_like', function (Blueprint $table) {
            //
        });
    }
}
