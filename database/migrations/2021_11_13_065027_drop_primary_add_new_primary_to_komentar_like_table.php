<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPrimaryAddNewPrimaryToKomentarLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komentar_like', function (Blueprint $table) {
            //
            $table->dropColumn('id');
            $table->primary(['user_id', 'komentar_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar_like', function (Blueprint $table) {
            //
        });
    }
}
