<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Social Media - Kita</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
 ============================================ -->
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900"
        rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bicon.min.css') }}">
    <!-- Flat Icon CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/flaticon.css') }}">
    <!-- audio & video player CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/plyr.css') }}">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/slick.min.css') }}">
    <!-- nice-select CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/nice-select.css') }}">
    <!-- perfect scrollbar css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/perfect-scrollbar.css') }}">
    <!-- light gallery css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/lightgallery.min.css') }}">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

</head>

<body>

    <!-- header area start -->
    <header>
        <div class="header-top sticky bg-white d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <!-- header top navigation start -->
                        <div class="header-top-navigation">
                            <nav>
                                <ul>
                                    <li class="active"><a href="#">home</a></li>
                                    <li class="msg-trigger"><a href="/postingan"
                                            class="btn btn-info btn-sm">Postingan</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- header top navigation start -->
                    </div>

                    <div class="col-md-2">
                        <!-- brand logo start -->
                        <div class="brand-logo text-center">
                            <a href="index.html">
                                <img src="{{ asset('assets/images/logo/logo-kita-1.png') }}" alt="brand logo">
                            </a>
                        </div>
                        <!-- brand logo end -->
                    </div>

                    <div class="col-md-5">
                        <div class="header-top-right d-flex align-items-center justify-content-end">

                            <!-- profile picture start -->
                            <div class="profile-setting-box">
                                <div class="profile-thumb-small">
                                    <a href="javascript:void(0)" class="profile-triger">
                                        <figure>
                                            <img src="assets/images/profile/profile-35x35-1.jpg" alt="profile picture">
                                        </figure>
                                    </a>
                                    <div class="profile-dropdown">
                                        <div class="profile-head">
                                            <h5 class="name"><a href="#">{{ $usercurrent->name }}</a></h5>
                                            <a class="mail" href="#">{{ $usercurrent->email }}</a>
                                        </div>
                                        <div class="profile-body">
                                            <ul>
                                                <li><a href="profile.html"><i class="flaticon-user"></i>Profile</a>
                                                </li>

                                            </ul>
                                            <ul>
                                                <li>
                                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                  document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}"
                                                        method="POST" style="display: none;">
                                                        @csrf
                                                    </form>


                                                    <!--  <a href="signup.html"><i class="flaticon-unlock"></i>Sing out</a>-->
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- profile picture end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header area end -->
    <!-- header area start -->
    <header>
        <div class="mobile-header-wrapper sticky d-block d-lg-none">
            <div class="mobile-header position-relative ">
                <div class="mobile-logo">
                    <a href="#">
                        <img src="{{ asset('assets/images/logo/logo-white.png') }}" alt="logo">
                    </a>
                </div>
            </div>
        </div>
    </header>
    <!-- header area end -->

    <main>

        <div class="main-wrapper pt-80">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="widget-area">
                            <!-- widget single item start -->
                            <div class="card card-profile widget-item p-0">
                                <div class="profile-banner">
                                    <figure class="profile-banner-small">
                                        <a href="profile.html">
                                            <img src="assets/images/banner/banner-small.jpg" alt="">
                                        </a>
                                        <a href="profile.html" class="profile-thumb-2">
                                            <img src="assets/images/profile/profile-midle-1.jpg" alt="">
                                        </a>
                                    </figure>
                                    <div class="profile-desc text-center">
                                        <h6 class="author"><a
                                                href="profile.html">{{ $usercurrent->name }}</a>
                                        </h6>
                                        <p class="list-subtitle"><a
                                                href="#">{{ $usercurrent->followings()->count() }}
                                                followings</a> |<a href="#">{{ $usercurrent->followers()->count() }}
                                                followers</a>
                                        </p>

                                        <p>{{ $profile->bio }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- widget single item start -->

                            <!-- widget single item start -->
                            <div class="card widget-item">
                                <h4 class="widget-title">Postingan you like</h4>
                                <div class="widget-body">
                                    @forelse ($postinganlike ??'' as $item_postingan)
                                        <ul class="like-page-list-wrapper">
                                            <li class="unorder-list">
                                                <!-- profile picture end -->
                                                <div class="profile-thumb">
                                                    <a href="#">
                                                        <figure class="profile-thumb-small">
                                                            <img src="assets/images/profile/profile-35x35-1.jpg"
                                                                alt="profile picture">
                                                        </figure>
                                                    </a>
                                                </div>
                                                <!-- profile picture end -->

                                                <div class="unorder-list-info">
                                                    <h3 class="list-title"><a
                                                            href="#">{{ $item_postingan->postingan->caption }}</a>
                                                    </h3>
                                                    <p class="list-subtitle"><a
                                                            href="#">{{ Str::limit($item_postingan->postingan->tulisan, 50) }}</a>
                                                    </p>
                                                </div>
                                                <button class="like-button active">
                                                    <img class="heart" src="assets/images/icons/heart.png"
                                                        alt="">
                                                    <img class="heart-color"
                                                        src="assets/images/icons/heart-color.png" alt="">
                                                </button>



                                            </li>
                                        </ul>
                                    @empty
                                        <ul class="like-page-list-wrapper">
                                            <li class="unorder-list">
                                                tidak ada postingan yang saya suka
                                            </li>
                                        </ul>
                                    @endforelse ( as )

                                </div>
                            </div>
                            <!-- widget single item end -->


                        </aside>
                    </div>

                    <div class="col-lg-6 order-1 order-lg-2">
                        <!-- share box start -->
                        <!-- share box end -->
                        <!-- post status start -->
                        <div class="card">
                            @forelse ($postingan as $item_postingan)
                                <!-- post title start -->
                                <div class="post-title d-flex align-items-center mt-2">
                                    <!-- profile picture end -->
                                    <div class="profile-thumb">
                                        <a href="#">
                                            <figure class="profile-thumb-middle">
                                                <img src="assets/images/profile/profile-small-1.jpg"
                                                    alt="profile picture">
                                            </figure>
                                        </a>
                                    </div>
                                    <!-- profile picture end -->

                                    <div class="posted-author">
                                        <h6 class="author"><a
                                                href="profile.html">{{ $item_postingan->user->name }}</a></h6>
                                        <span class="post-time">{{ $item_postingan->created_at }}</span>
                                    </div>

                                    <div class="post-settings-bar">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <div class="post-settings arrow-shape">
                                            <ul>
                                                @if (Auth::id() == $item_postingan->user_id)
                                                    <li><a href="/postingan/{{ $item_postingan->id }}"
                                                            class="btn btn-info btn-sm">Detail</a></li>
                                                    <li><a href="/postingan/{{ $item_postingan->id }}/edit"
                                                            class="btn btn-warning btn-sm">Edit</a></li>

                                                    <!--<li><button>edit postingan</button></li>
                                                    <li><button>delete postingan</button></li>-->
                                                @endif

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- post title start -->
                                <div class="post-content">
                                    <p class="post-desc">
                                        {{ $item_postingan->caption }}
                                    </p>
                                    <p class="post-desc">
                                        {{ $item_postingan->tulisan }}
                                    </p>
                                    <p class="post-desc">
                                        {{ $item_postingan->quote }}
                                    </p>
                                    <div class="post-thumb-gallery">
                                        <figure class="post-thumb img-popup">
                                            <a href="{{ asset("gambar/$item_postingan->gambar ") }}">
                                                <img src="{{ asset("gambar/$item_postingan->gambar") }}"
                                                    alt="post image">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="post-meta">
                                        <button class="post-meta-like">
                                            <i class="bi bi-heart-beat"></i>
                                            <span><small>{{ $item_postingan->postingan_like->where('postingan_id', $item_postingan->id)->count() }}
                                                    like
                                                    this and
                                                    {{ $item_postingan->komentar->where('postingan_id', $item_postingan->id)->count() }}
                                                    komentar</small></span>
                                            <strong>201</strong>
                                        </button>
                                        <ul class="comment-share-meta">
                                            <li>
                                                <div class="interaction">
                                                    <form action="/postingan_like" class="my-3" method="post">
                                                        @csrf
                                                        <input type="hidden" name="postingan_id"
                                                            value="{{ $item_postingan->id }}">
                                                        <button type="submit" class="like"><i
                                                                class="bi bi-like"></i><span>Like</span></button>
                                                    </form>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <button class="post-comment">
                                                    <i class="bi bi-chat-bubble"></i>
                                                    <span>41</span>
                                                </button>
                                            </li>-->
                                        </ul>
                                    </div>
                                    <div class="post-meta">
                                        <div class="row">
                                            komentar
                                            @forelse ($item_postingan->komentar as $item)
                                                <small><b>{{ $item->user->name }}</b></small>
                                                <p class="card-text"><small>{{ $item->komentar }}</small></p>
                                                <button class="post-meta-like">
                                                    <i class="bi bi-heart-beat"></i>
                                                    <span><small>{{ $item->komentar_like->where('komentar_id', $item->id)->count() }}
                                                            like
                                                            this komentar</small></span>
                                                    <strong>201</strong>
                                                </button>
                                                <div class="interaction">
                                                    <form action="/komentar_like" class="my-3" method="post">
                                                        @csrf
                                                        <input type="hidden" name="komentar_id"
                                                            value="{{ $item->id }}">
                                                        <button type="submit" class="like"><i
                                                                class="bi bi-like"></i><span>Like</span></button>
                                                    </form>
                                                </div>

                                            @empty
                                            @endforelse
                                            <div class="share-box-inner">
                                                <!-- profile picture end -->
                                                <div class="profile-thumb">
                                                    <a href="#">
                                                        <figure class="profile-thumb-middle">
                                                            <img src="assets/images/profile/profile-small-37.jpg"
                                                                alt="profile picture">
                                                        </figure>
                                                    </a>
                                                </div>
                                                <!-- profile picture end -->

                                                <!-- share content box start -->
                                                <div class="share-content-box w-100">
                                                    <form class="share-text-box" action="/komentar" method="POST">
                                                        @csrf
                                                        <textarea name="komentar" class="share-text-field"
                                                            aria-disabled="true" placeholder="Say Something"
                                                            data-bs-toggle="modal" data-bs-target="#textbox"
                                                            id="komentar"></textarea>
                                                        <input type="hidden" name="postingan_id"
                                                            value="{{ $item_postingan->id }}">
                                                        <button class="btn-share" type="submit"><i
                                                                class="bi bi-chat-bubble"></i></button>
                                                    </form>
                                                </div>
                                                <!-- share content box end -->
                                                <div class="modal fade" id="textbox" aria-labelledby="textbox">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Share Your Komentar</h5>
                                                                <button type="button" class="close"
                                                                    data-bs-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form class="share-text-box" action="/komentar"
                                                                method="POST">
                                                                @csrf

                                                                <div class="modal-body custom-scroll">
                                                                    <textarea name="komentar"
                                                                        class="share-field-big custom-scroll"
                                                                        placeholder="Say Something"></textarea>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="postingan_id"
                                                                        value="{{ $item_postingan->id }}">
                                                                    <button type="button" class="post-share-btn"
                                                                        data-bs-dismiss="modal">cancel</button>
                                                                    <button type="submit" class="post-share-btn">post
                                                                        komentar</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Modal end -->
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            @empty
                                <div class="post-content">
                                    <h1>Data postingan masih kosong</h1>
                                </div>
                            @endforelse ( as )
                        </div>
                        <!-- post status end -->
                    </div>
                    <div class="col-lg-3 order-3">
                        <aside class="widget-area">

                            <!-- widget single item start -->
                            <div class="card widget-item">
                                <h4 class="widget-title">Friends Zone</h4>
                                <div class="widget-body">
                                    <ul class="like-page-list-wrapper">

                                        @forelse ($user as $useritem)
                                            @if ($useritem->id != Auth::id())
                                                <li class="unorder-list">
                                                    <!-- profile picture end -->
                                                    <div class="profile-thumb">
                                                        <a href="#">
                                                            <figure class="profile-thumb-small">
                                                                <img src="assets/images/profile/profile-35x35-15.jpg"
                                                                    alt="profile picture">
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <!-- profile picture end -->

                                                    <div class="unorder-list-info">
                                                        <h3 class="list-title"><a
                                                                href="#">{{ $useritem->name }}</a>
                                                        </h3>
                                                        <p class="list-subtitle"><a
                                                                href="#">{{ $useritem->followings()->count() }}
                                                                followings</a> |<a
                                                                href="#">{{ $useritem->followers()->count() }}
                                                                followers</a>
                                                        </p>
                                                    </div>
                                                    <?php
                                                    //print_r($followinguser);
                                                    ?>

                                                    @if (in_array($useritem->id, $followinguser))
                                                        <button class="like-button active" disabled>
                                                            <img class="heart"
                                                                src="assets/images/icons/heart.png" alt="">
                                                            <img class="heart-color"
                                                                src="assets/images/icons/heart-color.png" alt="">
                                                        </button>

                                                    @else
                                                        <form action="/follower" class="my-3" method="post">
                                                            @csrf
                                                            <input type="hidden" name="user_id"
                                                                value="{{ $useritem->id }}">
                                                            <button type="submit" class="like-button"><img
                                                                    class="heart"
                                                                    src="assets/images/icons/heart.png" alt=""></button>
                                                        </form>
                                                    @endif

                                                </li>
                                            @endif
                                        @empty
                                            <li class="unorder-list">
                                                <h1>Data user lain masih kosong</h1>
                                            </li>
                                        @endforelse ( as )
                                    </ul>
                                </div>
                            </div>
                            <!-- widget single item end -->
                        </aside>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="bi bi-finger-index"></i>
    </div>
    <!-- Scroll to Top End -->

    <!-- footer area start -->
    <footer class="d-none d-lg-block">
        <div class="footer-area reveal-footer">
        </div>
    </footer>
    <!-- footer area end -->
    <!-- footer area start -->
    <footer class="d-block d-lg-none">
        <div class="footer-area reveal-footer">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="mobile-footer-inner d-flex">
                            <div class="mobile-frnd-search">
                                <button class="search-toggle-btn"><i class="flaticon-search"></i></button>
                            </div>
                            <div class="mob-frnd-search-inner">
                                <form class="mob-frnd-search-box d-flex">
                                    <input type="text" placeholder="Search Your Friends" class="mob-frnd-search-field">
                                </form>
                            </div>
                            <div class="card card-small mb-0 active-profile-mob-wrapper">
                                <div class="active-profiles-mob-wrapper slick-row-10">
                                    <div class="active-profile-mobile">
                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="#">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-1.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->

                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="javascript:void(0)">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-8.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->

                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="javascript:void(0)">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-2.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->

                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="javascript:void(0)">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-3.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->

                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="javascript:void(0)">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-4.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->

                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="javascript:void(0)">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-5.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->

                                        <!-- profile picture end -->
                                        <div class="single-slide">
                                            <div class="profile-thumb active profile-active">
                                                <a href="javascript:void(0)">
                                                    <figure class="profile-thumb-small profile-active">
                                                        <img src="assets/images/profile/profile-small-9.jpg"
                                                            alt="profile picture">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- profile picture end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="{{ asset('assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <!-- jQuery JS -->
    <script src="{{ asset('assets/js/vendor/jquery-3.6.0.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('assets/js/vendor/bootstrap.bundle.min.js') }}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('assets/js/plugins/slick.min.js') }}"></script>
    <!-- nice select JS -->
    <script src="{{ asset('assets/js/plugins/nice-select.min.js') }}"></script>
    <!-- audio video player JS -->
    <script src="{{ asset('assets/js/plugins/plyr.min.js') }}"></script>
    <!-- perfect scrollbar js -->
    <script src="{{ asset('assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <!-- light gallery js -->
    <script src="{{ asset('assets/js/plugins/lightgallery-all.min.js') }}"></script>
    <!-- image loaded js -->
    <script src="{{ asset('assets/js/plugins/imagesloaded.pkgd.min.js') }}"></script>
    <!-- isotope filter js -->
    <script src="{{ asset('assets/js/plugins/isotope.pkgd.min.js') }}"></script>
    <!-- Main JS -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>
