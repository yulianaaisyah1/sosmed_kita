@extends('layout.master')

@section('judul')
    Form
@endsection

@section('judul_detail')
    Edit Data Postingan
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/iohlwx7o80481berg85vr5zwcrq0e3oociumumbydw359h9b/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>

@endpush



@section('content')
    <form action="/postingan/{{ $postingan->id }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="judul" class="form-label">Judul</label>
            <input type="text" class="form-control" name="caption" value="{{ $postingan->caption }}">
        </div>
        @error('caption')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="tulisan" class="form-label">Tulisan</label>
            <textarea class="form-control" name="tulisan" cols="50" rows="10">{{ $postingan->tulisan }}</textarea>
        </div>
        @error('tulisan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="quote" class="form-label">Quote</label>
            <textarea class="form-control" name="quote" cols="50" rows="10">{{ $postingan->quote }}</textarea>
        </div>
        @error('quote')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="gambar" class="form-label">gambar</label>
            <input type="file" class="form-control" name="gambar">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
