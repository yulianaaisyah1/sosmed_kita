@extends('layout.master')

@section('judul')
    List
@endsection

@section('judul_detail')
    Data Postingan
@endsection

@section('content')
    @auth
        <a href="/postingan/create" class="btn btn-success mb-3">Tambah Data</a>
    @endauth

    <div class="row">
        @forelse ($postingan as $item)
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('gambar/' . $item->gambar) }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h3>{{ $item->caption }}</h3>
                        <p class="card-text">{{ Str::limit($item->tulisan, 30) }}</p>
                        <p class="card-text">{{ Str::limit($item->quote, 30) }}</p>
                        <p class="card-text">{{ $item->komentar->count() }} komentar</p>
                        @auth
                            <form action="/postingan/{{ $item->id }}" method="post">
                                @csrf
                                @method("delete")
                                <a href="/postingan/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/postingan/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                @if ($item->komentar->count() == 0)
                                    <input type=submit class="btn btn-danger btn-sm" value="Delete">
                                @endif
                            </form>
                        @endauth
                        @guest
                            <a href="/postingan/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                        @endguest
                    </div>
                </div>
            </div>
        @empty
            <h1>Data postingan masih kosong</h1>
        @endforelse ( as )
    </div>


@endsection
