@extends('layout.master')

@section('judul')
    Show
@endsection

@section('judul_detail')
    postingan ke {{ $postingan->id }}
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/iohlwx7o80481berg85vr5zwcrq0e3oociumumbydw359h9b/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>

@endpush

@section('content')
    <img src="{{ asset('gambar/' . $postingan->gambar) }}" alt="">
    <h4>{{ $postingan->captioon }}</h4>
    <p>{{ $postingan->tulisan }}</p>
    <p>{{ $postingan->quote }}</p>
    <p>tanggal dibuat: {{ $postingan->created_at }}</p>

    <h3>komentar</h3>
    @forelse ($postingan->komentar as $item)
        <div class="card">
            <div class="card-body">
                <small><b>{{ $item->user->name }}</b></small>
                <p class="card-text">{{ $item->komentar }}</p>
            </div>
        </div>
    @empty
        <h3>tidak ada komentar</h3>
    @endforelse
    <form action="/komentar" class="my-3" method="post">
        @csrf
        <div class="form-group">
            <label for="komentar" class="form-label">Komentar</label>
            <textarea class="form-control" name="komentar" cols="50" rows="10"></textarea>
        </div>
        <input type="hidden" name="postingan_id" value="{{ $postingan->id }}">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>



    <a href="\postingan">kembali</a>

@endsection
