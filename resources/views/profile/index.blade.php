@extends('layout.master')

@section('judul')
    Form
@endsection

@section('judul_detail')
    Update Profile
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/iohlwx7o80481berg85vr5zwcrq0e3oociumumbydw359h9b/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>

@endpush

@section('content')
    <form action="/profile/{{ $profile->id }}" method="post">
        @csrf
        @method('put')
        <!--tampilkan data user pakai belongsto-->
        <div class="form-group">
            <label class="form-label">nama</label>
            <input type="text" class="form-control" disabled value="{{ $profile->user->name }}">
        </div>
        <div class="form-group">
            <label class="form-label">email</label>
            <input type="text" class="form-control" disabled value="{{ $profile->user->email }}">
        </div>
        <!---------- tampilkan data user -->
        <div class="form-group">
            <label for="umur" class="form-label">umur</label>
            <input type="number" class="form-control" name="umur" value="{{ $profile->umur }}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="alamat" class="form-label">alamat</label>
            <textarea class="form-control" name="alamat" cols="50" rows="10">{{ $profile->alamat }}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="bio" class="form-label">bio</label>
            <textarea class="form-control" name="bio" cols="50" rows="10">{{ $profile->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
