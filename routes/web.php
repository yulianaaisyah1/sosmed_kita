<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
//Route::get('/home', 'HomeController@index')->name('home');




Auth::routes();


Route::resource('/postingan_like', 'PostinganLikeController')->only([
    'store'
]);

Route::resource('/komentar_like', 'KomentarLikeController')->only([
    'store'
]);

Route::resource('/komentar', 'KomentarController')->only([
    'store'
]);

Route::resource('/follower', 'FollowUserController')->only([
    'store'
]);


Route::middleware('auth')->group(function () {
    Route::resource('/postingan', 'PostinganController');

    Route::resource('/profile', 'ProfileController')->only([
        'index', 'update'
    ]);    
});
